<div class="container maq-container">
      <?php 
        if(have_rows('carrossel_equip_pascarregadeiras')) : 
	  ?>
		<h3 class="maq-title" id="pa-carregadeira">
      <i class="fa fa-angle-right"></i> PÁS CARREGADEIRAS
		</h3>
	  <?php
            while(have_rows('carrossel_equip_pascarregadeiras')) : 
                the_row();   
                $attachment_id = get_sub_field('pascarregadeiras_imagem');
                $imagem = wp_get_attachment_image_src( $attachment_id, 'equip-carrossel' );
       ?>
        <div class="col-md-4 col-sm-6 col-xs-12 text-center">
          <div class="chose_box">
            <img src="<?php echo $imagem[0] ?>"/>
            <div class="content">
              <h4 class="maq-item-title"><strong><?php the_sub_field('pascarregadeiras_marca') ?></strong> - <?php the_sub_field('pascarregadeiras_modelo') ?></h4>
              <ul>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/weight-kg-e1615414781710.png" alt="peso"/>
                  <?php the_sub_field('pascarregadeiras_peso') ?>
                </li>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/engine.png" alt="força"/>
                  <?php the_sub_field('pascarregadeiras_motor') ?>
                </li>
              </ul>
              <p class="text-left">
                <strong>Cabine:</strong>
                <br/><?php the_sub_field('pascarregadeiras_cabine') ?>
              </p>
              <p class="text-left">
                <strong>Capacidade da Caçamba:</strong>
                <br/><?php the_sub_field('pascarregadeiras_capacidade') ?>
              </p>
              <p class="text-left">
                <strong>Altura Máxima de Descarga:</strong>
                <br/><?php the_sub_field('pascarregadeiras_alt_max_desc') ?>
              </p>
              <p class="text-left">
                <strong>Carga Máxima Articulada:</strong>
                <br/><?php the_sub_field('pascarregadeiras_carga_maxima') ?>
              </p>
            </div>
            <div class="botao-orcamento">
              <div class="buttom" style="margin: 30px 0px;">
                  <a href="https://api.whatsapp.com/send?phone=5527998401521&text=Olá! Quero fazer um orçamento desse produto: PÁ CARREGADEIRA" class="btn btn-success" target="_blank">Solicitar Orçamento</a>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; 
			else:
			endif; 
	  ?>
  </div>