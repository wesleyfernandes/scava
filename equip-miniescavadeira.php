  <div class="container maq-container">
      <?php 
        if(have_rows('carrossel_equip_escavadeira')) : 
	  ?>
		<h3 class="maq-title"  id="miniescavadeira">
      <i class="fa fa-angle-right"></i> MINIESCAVADEIRAS
		</h3>
	  <?php
            while(have_rows('carrossel_equip_miniescavadeira')) : 
                the_row(); 
                $attachment_id = get_sub_field('miniescavadeira_imagem');
                $imagem = wp_get_attachment_image_src( $attachment_id, 'equip-carrossel' );
       ?>
        <div class="col-md-4 col-sm-6 col-xs-12 text-center">
          <div class="chose_box">
            <img src="<?php echo $imagem[0] ?>"/>
            <div class="content">
              <h4 class="maq-item-title"><strong><?php the_sub_field('miniescavadeira_marca') ?></strong> - <?php the_sub_field('miniescavadeira_modelo') ?></h4>
              <ul>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/weight-kg-e1615414781710.png" alt="peso"/>
                  <?php the_sub_field('miniescavadeira_peso') ?>
                </li>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/engine.png" alt="força"/>
                  <?php the_sub_field('miniescavadeira_motor') ?>
                </li>
              </ul>
              <p class="text-left implementos">
                <strong style="font-size:14px">Implementos:</strong>
                <br/><?php the_sub_field('miniescavadeira_implementos') ?>
              </p>
              <p class="text-left">
                <strong>Capacidade da Caçamba:</strong>
                <br/><?php the_sub_field('miniescavadeira_capacidade') ?>
              </p>
              <p class="text-left">
                <strong>Profundidade de Escavação:</strong>
                <br/><?php the_sub_field('miniescavadeira_profundidade_de_escavacao') ?>
              </p>
              <p class="text-left">
                <strong>Altura Máxima de Descarga:</strong>
                <br/><?php the_sub_field('miniescavadeira_altura_maxima_de_descarga') ?>
              </p>
              <p class="text-left">
                <strong>Dimensões:</strong>
                <br/><?php the_sub_field('miniescavadeira_dimensoes') ?>
              </p>
              <p class="text-left">
                <strong>Alcance Horizontal:</strong>
                <br/><?php the_sub_field('miniescavadeira_alcance_horizontal') ?>
              </p>
            </div>
            <div class="botao-orcamento">
              <div class="buttom" style="margin: 30px 0px;">
                  <a href="https://api.whatsapp.com/send?phone=5527998401521&text=Olá! Quero fazer um orçamento desse produto: MINIESCAVADEIRA" class="btn btn-success" target="_blank">Solicitar Orçamento</a>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; 
			else:
			endif; 
	  ?>
  </div>