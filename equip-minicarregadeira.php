  <div class="container maq-container">
      <?php 
        if(have_rows('carrossel_equip_minicarregadeira')) : 
	  ?>
		<h3 class="maq-title" id="minicarregadeira">
      <i class="fa fa-angle-right"></i> MINICARREGADEIRAS
		</h3>
	  <?php
            while(have_rows('carrossel_equip_minicarregadeira')) : 
                the_row(); 
                $attachment_id = get_sub_field('minicarregadeira_imagem');
                $imagem = wp_get_attachment_image_src( $attachment_id, 'equip-carrossel' );
       ?>
        <div class="col-md-4 col-sm-6 col-xs-12 text-center">
          <div class="chose_box">
            <img src="<?php echo $imagem[0] ?>"/>
            <div class="content">
              <h4 class="maq-item-title"><strong><?php the_sub_field('minicarregadeira_marca') ?></strong> - <?php the_sub_field('minicarregadeira_modelo') ?></h4>
              <ul>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/weight-kg-e1615414781710.png" alt="peso"/>
                  <?php the_sub_field('minicarregadeira_peso') ?>
                </li>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/engine.png" alt="força"/>
                  <?php the_sub_field('minicarregadeira_motor') ?>
                </li>
              </ul>
              <p class="text-left implementos">
                <strong style="font-size:14px">Implementos:</strong>
                <br/><?php the_sub_field('minicarregadeira_implementos') ?>
              </p>
              <p class="text-left">
                <strong style="font-size:14px">Cabine:</strong>
                <br/><?php the_sub_field('minicarregadeira_cabine') ?>
              </p>
              <p class="text-left">
                <strong>Altura Máxima de Descarga:</strong>
                <br/><?php the_sub_field('minicarregadeira_altura_maxima_de_descarga') ?>
              </p>
              <p class="text-left">
                <strong>Capacidade de Carga:</strong>
                <br/><?php the_sub_field('minicarregadeira_capacidade') ?>
              </p>
              <p class="text-left">
                <strong>High Flow:</strong>
                <br/><?php the_sub_field('minicarregadeira_highflow') ?>
              </p>
            </div>
            <div class="botao-orcamento">
              <div class="buttom" style="margin: 30px 0px;">
                  <a href="https://api.whatsapp.com/send?phone=5527998401521&text=Olá! Quero fazer um orçamento desse produto: MINICARREGADEIRA" class="btn btn-success" target="_blank">Solicitar Orçamento</a>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; 
			else:
			endif; 
	  ?>
  </div>