<?php

get_header();
?>


<!--========== SLIDER ==========-->
<div id="home_2">
  <section id="index-banner" data-vide-bg="<?php the_field('home_banner_video_'); ?>">
    <div class="bg-opacity-layer"></div>
    <div id="banner_2" class="home-center">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center mt150 mb55">
            <h1 class="text-uppercase"><?php the_field('home_banner_titulo'); ?></h1>
            <h4 class="text-uppercase mt15 mb45"><?php the_field('home_banner_subtitulo'); ?></h4>
            <div class="pur_button-white"> <a href="<?php the_field('home_banner_link_botao'); ?>" class="scroll"><?php the_field('home_banner_texto_botao'); ?></a> </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--========== #/SLIDER ==========-->


<!--========== ABOUT US ==========-->
<section class="page-block" id="about">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
            <h2 class="text-uppercase"><?php the_field('titulo_empresa');?></h2>
            <p><?php the_field('subtitulo_empresa');?></p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5 col-sm-5 col-xs-12 text-center">
        <div class="aboutImg"> 
            <img alt="About" src="<?php the_field('imagem_destaque_empresa');?>" data-wow-offset="300" class="img-responsive" style="visibility: visible;"> <img alt="Seperator shadow" src="<?php echo get_bloginfo('template_url')?>/assets/img/seperator-shadow.png" class="shadow img-responsive"> </div>
      </div>
      
      
	    <div class="col-md-7 col-sm-7 col-xs-12">
        <div class="row">
          <div class="col-md-12">
            <h3 class="text-uppercase">Saiba mais!</h3>
          </div>
          <div class="col-md-12">
            <p style="text-align:justify"><?php the_field('historia_da_empresa');?></p>
            <!-- 
			<a href="#" class="btn-light">Read more</a>
			!-->
			    </div>
        </div>
      </div>
    </div>    
  </div>
</section>
<!--========== #/ABOUT US ==========-->

<!--========== WHY CHOSE US ==========-->
<section id="index1-chose">
  <div class="container">
      <div class="row">
		    <div class="col-md-12 text-center index1-heading">
          <h2 class="text-uppercase"><?php the_field('equip_titulo');?></h2>
          <p><?php the_field('equip_subtitulo');?></p>
        </div>
	    </div>
	  
      <!-- ESCAVADEIRAS !-->
      <?php include get_template_directory().'/equip-escavadeiras.php'; ?>
      <!-- FIM ESCAVADEIRAS !-->
        
      <!-- RETROESCAVADEIRAS !-->
      <?php include get_template_directory().'/equip-retroescavadeiras.php'; ?>  
      <!-- FIM RETROESCAVADEIRAS !-->
        
      <!-- ROLOS COMPACTADORES !-->
      <?php include get_template_directory().'/equip-roloscompactadores.php'; ?> 
      <!-- FIM ROLOS COMPACTADORES !-->
        
      <!-- MINIESCAVADEIRAS !-->
      <?php include get_template_directory().'/equip-miniescavadeira.php'; ?> 
      <!-- FIM MINIESCAVADEIRAS !-->
        
      <!-- MINICARREGADEIRAS !-->
      <?php include get_template_directory().'/equip-minicarregadeira.php'; ?> 
      <!-- FIM MINICARREGADEIRAS !-->
        
      <!-- CAMINHÕES !-->
      <?php include get_template_directory().'/equip-caminhoes.php'; ?>
      <!-- FIM CAMINHÕES !-->

      <!-- TRATORES DE ESTEIRA !-->
      <?php include get_template_directory().'/equip-tratoresdeesteira.php'; ?>
      <!-- FIM TRATORES DE ESTEIRA !-->

      <!-- PÁS CARREGADEIRAS !-->
      <?php include get_template_directory().'/equip-pascarregadeiras.php'; ?>
      <!-- FIM PÁS CARREGADEIRAS !-->

      <!-- MOTONIVELADORAS !-->
      <?php include get_template_directory().'/equip-motoniveladoras.php'; ?>
      <!-- FIM MOTONIVELADORAS !-->
  </div>
</section>
<!--========== #/WHY CHOSE US ==========-->

<!--========== SERVICES ==========-->

<section id="index1-services">

	<div class="container over-hide" >
    <div class="row" >
      <div class="col-md-12 text-center index1-heading">
        <h2 class="text-uppercase"><?php the_field('titulo_servicos') ?></h2>
        <p><?php the_field('subtitulo_servicos') ?></p>
      </div>
    </div>

    <div class="row">
      <div id="services_index1" class="owl-carousel">
      <?php 
        if(have_rows('carrossel_servicos')) : while(have_rows('carrossel_servicos')) : the_row(); 
        $attachment_id = get_sub_field('carrossel_servicos_imagem');
        $imagem = wp_get_attachment_image_src( $attachment_id, 'services-carrossel' );
      ?>
        <div class="item">
          <div class="index1-services-box text-center">
            <div class="servc_img">
              <img src="<?php echo $imagem[0];?>" alt="image">
            </div>
            <div class="servc_detail">
              <a><h3><?php the_sub_field('carrossel_servicos_titulo') ?></h3></a>
              <p><?php the_sub_field('carrossel_servicos_texto') ?></p>
            </div>
          </div>
        </div>
      <?php endwhile; endif; ?>
      </div>
    </div>

  </div>  
</section>
<!--========== #/SERVICES ==========-->

<!--========== RECENT PROJECTS ==========-->
<section class="gallery-section" id="gallery-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center index1-heading">
        <h2 class="text-uppercase"><?php the_field('recentes_titulo') ?></h2>
        <p><?php the_field('recentes_subtitulo') ?></p>
      </div>
    </div>
  </div>
  <div class="container-fluid"> 
    <!--Carousel-->
    <div id="gallery-slider" class="owlCarouse">
    <?php 
      if(have_rows('carrossel_recentes')) : while(have_rows('carrossel_recentes')) : the_row(); 
      $attachment_id = get_sub_field('carrossel_recentes_imagem');
      $imagem = wp_get_attachment_image_src( $attachment_id, 'recentes-carrossel' );
    ?>
      <article class="slide-item">
        <figure class="image-box">
          <a class="" title="<?php the_sub_field('carrossel_recentes_titulo') ?>"><img src="<?php echo $imagem[0]; ?>" alt=""></a>
        </figure>
        <?php
        
        ?>
        <a class="overlay" title="<?php the_sub_field('carrossel_recentes_titulo') ?>"></a>
        <div class="item-caption">
          <h4><strong><?php the_sub_field('carrossel_recentes_categoria') ?></strong></h4>
          <p><?php the_sub_field('carrossel_recentes_titulo') ?></p>
        </div>
      </article>
    <?php endwhile; endif; ?>
    </div>
  </div>
</section>
<!--========== #/RECENT PROJECTS ==========-->


<!--========== CONTACT ==========--> 
<section id="index1-conatcat">
  <div class="container">
    <div class="row">
      <div class="col-md-12 index1-heading">
        <h2 class="text-uppercase">Fale <span class="color_red bolder">Conosco</span></h2>
        <p>Aqui você encontra todos os meios de contato com nossa empresa.<br>
          Teremos o maior prazer em atende-lo!</p>
      </div>
    </div>
  
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="row">
          <div class="col-md-12">
              <div class="index1_adress">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <p>
                      <span><?php the_field('endereco_da_empresa','options');?></span>
                      <span><?php the_field('bairro_da_empresa','options');?> - <?php the_field('cidade_e_estado_da_empresa','options');?></span>
                      <span>CEP: <?php the_field('cep_da_empresa','options');?></span>
                  </p>
              </div>
          </div>
          
          <div class="col-md-12">
              <div class="index1_adress">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                <p>
                    <span><a href="mailto:<?php the_field('email_da_empresa','options');?>"><?php the_field('email_da_empresa','options');?></a></span>
                  </p>
              </div>
          </div>
          
          <div class="col-md-12">
              <div class="index1_adress">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <p>
                    <span><a href="tel:+<?php the_field('telefone_fixo_da_empresa','options');?>" title="<?php the_field('telefone_fixo_da_empresa','options');?>"><?php the_field('telefone_fixo_da_empresa','options');?></a></span>
                      <span><a href="https://wa.me/<?php the_field('link_whatsapp_da_empresa','options');?>" title="whatsapp" target="_blank"><?php the_field('celular_whatsapp_da_empresa','options');?> (whatsapp)</a></span>
                  </p>
              </div>
          </div>  
        </div>
      </div>
      <!-- Form WP !-->
      <form class="index1-form" action="" method="">
        <?php echo do_shortcode('[contact-form-7 id="3345" title="Form Contato"]'); ?>
      </form>
      <!-- End Form WP !-->

    </div>
  </div>
</section>
<div class="index1-map">
    <div id="map">
		<iframe src="<?php the_field('link_incorporado_google_maps','options');?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>
<!--========== #/CONTACT ==========-->  

<?php
get_footer();
