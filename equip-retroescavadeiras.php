  <div class="container maq-container">
      <?php 
        if(have_rows('carrossel_equip_restroescavadeira')) : 
	  ?>
		<h3 class="maq-title" id="retroescavadeira">
      <i class="fa fa-angle-right"></i> RETROESCAVADEIRAS
		</h3>
	  <?php
            while(have_rows('carrossel_equip_restroescavadeira')) : 
                the_row(); 
                $attachment_id = get_sub_field('restroescavadeira_imagem');
                $imagem = wp_get_attachment_image_src( $attachment_id, 'equip-carrossel' );
       ?>
        <div class="col-md-4 col-sm-6 col-xs-12 text-center">
          <div class="chose_box">
            <img src="<?php echo $imagem[0] ?>"/>
            <div class="content">
              <h4 class="maq-item-title"><?php the_sub_field('restroescavadeira_marca') ?> - <?php the_sub_field('restroescavadeira_modelo') ?></h4>
              <ul>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/weight-kg-e1615414781710.png" alt="peso"/>
                  <?php the_sub_field('restroescavadeira_peso') ?>
                </li>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/engine.png" alt="força"/>
                  <?php the_sub_field('restroescavadeira_motor') ?>
                </li>
              </ul>
              <p class="text-left implementos">
                <strong style="font-size:14px">Implementos:</strong>
                <br/><?php the_sub_field('restroescavadeira_implementos') ?>
              </p>
              <p class="text-left">
                <strong style="font-size:14px">Cabine:</strong>
                <br/><?php the_sub_field('restroescavadeira_cabine') ?>
              </p>
              <p class="text-left">
                <strong>Largura da Caçamba Retro:</strong>
                <br/><?php the_sub_field('restroescavadeira_largura_cacamba_retro') ?>
              </p>
              <p class="text-left">
                <strong>Tração:</strong>
                <br/><?php the_sub_field('restroescavadeira_tracao') ?>
              </p>
            </div>
            <div class="botao-orcamento">
              <div class="buttom" style="margin: 30px 0px;">
                  <a href="https://api.whatsapp.com/send?phone=5527998401521&text=Olá! Quero fazer um orçamento desse produto: RETROESCAVADEIRA" class="btn btn-success" target="_blank">Solicitar Orçamento</a>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; 
			else:
			endif; 
	  ?>
  </div>