<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Tema_Teste
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="profile" href="https://gmpg.org/xfn/11" />

	<!--========== REQUIRED CSS ==========-->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css?id=<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css?id=<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/owl.carousel.css?id=<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/jquery.fancybox.css?id=<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/settings.css?id=<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/color.css?id=<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/style.css?id=<?php echo time(); ?>">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <!--========== #/REQUIRED CSS ==========-->

	<?php wp_head(); ?>
</head>

<body>
<?php wp_body_open(); ?>

<!--========== BACK TO TOP ==========--> 
<a class="scrollup">Topo</a> 
<!--========== #/BACK TO TOP ==========-->
<div class="supbar">
        <div class="container">
            <div class="supbar_content">
                <div class="supbar_tel">
                    <a class="supbar_tel_link" href="tel:<?php the_field('telefone_fixo_da_empresa','options');?>" target="_blank">
                        <i class="icone fa fa-phone" aria-hidden="true"></i>
                        <span class="supbar_tel_text"><?php the_field('telefone_fixo_da_empresa','options');?></span>
                    </a>
                </div>
                <div class="supbar_whatsapp">
                    <a class="supbar_whatsapp_link" href="https://wa.me/<?php the_field('link_whatsapp_da_empresa','options');?>" target="_blank">
                        <i class="icone fa fa-whatsapp" aria-hidden="true"></i>
                        <span class="supbar_whatsapp_text"><?php the_field('celular_whatsapp_da_empresa','options');?></span>
                    </a>
                </div>
                <div class="supbar_email">
                    <a class="supbar_email_link" href="mailto:<?php the_field('email_da_empresa','options');?>" target="_blank">
                        <i class="icone fa fa-envelope" aria-hidden="true"></i>
                        <span class="supbar_email_text"><?php the_field('email_da_empresa','options');?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
<!--========== HEADER ==========-->
 <!-- header -->
  <header id="header_9"> 
    <!-- Top bar -->
    <div class="topbar">
        <div class="container">
          <div class="row" >
          
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            	<div class="logo"> <a href="/"><?php the_custom_logo(); ?></a> </div>
            </div>
            
          </div>
        </div>
      </div>
    
    <!-- Logo -->
    <div class="container">    
    <!-- Navigation -->
        <div id="navigation" class="navigation affix-top" data-offset-top="2" data-spy="affix">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <nav class="navbar navbar-default default top-nav-collapse"> 
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                     </div>
                  
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <span>
                          <img src="https://scava.com.br/wp-content/uploads/2021/02/logomini.png" alt="logo">
                      </span>
                    <?php
                      wp_nav_menu(
                        array(
                          'theme_location'    => 'menu-1',
                          'menu_class'        => 'main-menu nav navbar-nav',
                          'list_item_class'   => 'nav-item',
                          'link_class'        => 'scroll',
                          'items_wrap'        => '<ul id="%1$s" class="%2$s" tabindex="0">%3$s</ul>'
                        )
                      );
                    ?>
                  </div>
                  <!-- /.navbar-collapse -->
                  
                  </nav>
              </div>
              
            </div>
          </div>
         </div>
     </div>
  </header>
<!--========== #/HEADER ==========-->

