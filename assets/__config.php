<?php 
/**
 * @autor: Evangelios - CMS Para Igrejas e Ministérios
 * @nome: Weslley Dutra - contato@agenciakairos.com.br
 */

ini_set('upload_max_filesize', '100M');
ini_set('default_charset', 'utf-8');
ini_set('magic_quotes_runtime', 0);
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

if(!headers_sent())
	header('Content-Type: text/html; charset=utf-8');
	
//Conexão com host local 

$host	  = 'localhost';
$usuario  = 'kebaseco_user';
$senha_db = 'kp]o=ovTRHh9';
$banco  = 'kebaseco_bd';

/*
$host	  = 'mysql.kebase.com.br';
$usuario  = 'kebase';
$senha_db = '2018kebase';
$banco	  = 'kebase';
*/

//Inicia conexão	
@$conexao	=	mysql_connect($host, $usuario, $senha_db);
				mysql_select_db($banco, $conexao) or die (mysql_error($conexao));
				
//Versão do sistema
define('VERSAO_SISTEMA','v1.1');
//Idioma padrão
define('IDIOMA_PADRAO','pt-BR');
//URL Atual
define('URL_ATUAL','//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
//Tabelas MYSQL
define('AGENDA','tb_agenda');
define('ANIVERSARIANTES','tb_aniversariantes');
define('ARQUIVOS','tb_arquivos');
define('ARQUIVOS_DESCRICAO','tb_arquivos_descricao');
define('ARTIGOS','tb_artigos');
define('AUDIOS','tb_audios');
define('AUDIOS_DESCRICAO','tb_audios_descricao');
define('PASTAS','tb_arquivos_pasta');
define('PASTAS_DESCRICAO','tb_arquivos_pasta_descricao');
define('BANNERS','tb_banners');
define('BOLETINS','tb_boletins');
define('COMENTARIOS','tb_comentarios');
define('CONFIGURACOES','tb_configuracoes');
define('CONTADOR','tb_contador');
define('CURRICULOS','tb_curriculos');
define('CURRICULOS_AREAS','tb_curriculos_areas');
define('CLIENTES','tb_clientes');
define('EMAILS','tb_emails');
define('ENQUETES','tb_enquetes');
define('ENQUETES_IPS','tb_enquetes_ips');
define('GALERIAS','tb_galeria');
define('GALERIAS_IMAGENS','tb_galeria_imagens');
define('LINKS','tb_links');
define('IDIOMAS','tb_linguagens');
define('PAGINAS','tb_paginas');
define('PAGINAS_DESCRICAO','tb_paginas_descricao');
define('PAGINAS_ADM','tb_paginas_adm');
define('PAGINAS_GALERIAS','tb_paginas_galerias');
define('PAGINAS_PERMITIDAS','tb_paginas_permitidas');
define('PUBLICACOES','tb_publicacoes');
define('PUBLICACOES_DESCRICAO','tb_publicacoes_descricao');
define('PUBLICACOES_CATEGORIAS','tb_publicacoes_categorias');
define('PUBLICACOES_CATEGORIAS_DESCRICAO','tb_publicacoes_categorias_descricao');
define('USUARIOS','tb_usuarios');
define('USUARIOS_ONLINE','tb_usuarios_online');
define('SESSAO','tb_sessao');
define('VIDEOS','tb_videos');
define('VIDEOS_DESCRICAO','tb_video_descricao');
define('LOGS_SISTEMA','tb_logs_sistema');
define('CATEGORIAS_PRODUTOS','tb_categorias');
define('PRODUTOS','tb_produtos');
define('PRODUTOS_GALERIAS','tb_produtos_galerias');
define('CATEGORIA_PRODUTOS','tb_categorias');
define('DISTRIBUIDORES','tb_distribuidores');

//Definindo configurações para o site
$slq_config = mysql_query("SELECT * FROM ". CONFIGURACOES ."")or die("<strong>Erro: </strong>" .mysql_error());
$config = mysql_fetch_assoc($slq_config);
					
//Definições das configurações do site
define('TITULO_SITE',utf8_encode($config['titulo_site']));
define('DESCRICAO_SITE',$config['descricao']);
define('PALAVRAS_CHAVES',$config['palavras_chaves']);
define('TEXTO_PARABENS',$config['texto_parabens']);
define('ENDERECO_SITE',$config['endereco_site']);
define('EMAIL_PRINCIPAL',$config['email_principal']);
define('NOME_EMPRESA',$config['empresa']);
define('ENDERECO_EMPRESA',$config['endereco']);
define('CIDADE_BAIRRO_EMPRESA',$config['cidade_bairro']);
define('ESTADO_EMPRESA',$config['estado']);
define('RESPONSAVEL_EMPRESA',$config['responsavel']);
define('TELEFONE_EMPRESA',$config['telefone']);

//Mostrar banners em todas as páginas
$mostrar_banner_nas_paginas = false;

//Tamanho das imagens
define('LARGURA_IMAGEM_GALERIAS',640);
define('LARGURA_ALTURA_IMAGEM_GALERIAS_THUMBS',360);

//Tamanho dos banners
define('LARGURA_BANNER_TOPO',1000);
define('ALTURA_BANNER_TOPO',360);
define('LARGURA_BANNER_LATERAL',160);
define('ALTURA_BANNER_LATERAL',190);
define('LARGURA_BANNER_CENTRAL',1200);
define('ALTURA_BANNER_CENTRAL',400);

//Tamanho das fotos dos links
define('LARGURA_ALTURA_FOTO_LINKS',69);

//Texto rodape site adm
define('TEXTO_DESCRICAO','<br/>Gerenciador Kairós '. VERSAO_SISTEMA .' - Gerenciador de conteúdos para sites <br/> Licenciado para <strong>'. NOME_EMPRESA .'</strong> ');

//Configuração da conta de e-mail - servidor SMTP (Envio de e-mail autenticado)
define('HOST',$config['host']);
define('EMAIL_SECUNDARIO',$config['email_secundario']);
define('SENHA_EMAIL_SECUNDARIO',$config['senha_email_secundario']);
?>	