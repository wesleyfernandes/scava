  <div class="container maq-container">
    <?php 
      if(have_rows('carrossel_equip_caminhoes')) : 
	  ?>
		<h3 class="maq-title" id="caminhoes">
      <i class="fa fa-angle-right"></i> CAMINHÕES
		</h3>
    <div class="equip-content-item">
	  <?php
        while(have_rows('carrossel_equip_caminhoes')) : 
          the_row(); 
          $attachment_id = get_sub_field('caminhoes_imagem');
          $imagem = wp_get_attachment_image_src( $attachment_id, 'equip-carrossel' );
       ?>
       
        <div class="col-md-4 col-sm-6 col-xs-12 text-center">
          <div class="chose_box">
            <img src="<?php echo $imagem[0] ?>"/>
            <div class="content">
              <p class="maq-item-top-title">Caminhão <?php the_sub_field('caminhoes_modelo_tipo') ?></p>
              <h4 class="maq-item-title" style="margin-top: 10px;">
                <strong><?php the_sub_field('caminhoes_marca')?></strong>
                <p><?php the_sub_field('caminhoes_modelo')?></p>
              </h4>
              <?php 
                $caminhao_tipo = get_sub_field('caminhoes_modelo_tipo');
                if($caminhao_tipo == 'Basculante'):
                ?>
                <p class="text-left">
                  <strong>Volume de carga:</strong>
                  <br/><?php the_sub_field('caminhoes_volume_de_carga') ?>
                </p>
                <p class="text-left">
                  <strong>Modelos disponíveis:</strong>
                  <br/><?php the_sub_field('caminhoes_modelos_disponiveis') ?>
                </p>
                <?php
                elseif($caminhao_tipo == 'Munck'):
                ?>
                <ul>
                  <li>
                    <img src="https://scava.com.br/wp-content/uploads/2021/03/engine.png" alt="força"/>
                    <?php the_sub_field('caminhoes_motor') ?>
                  </li>
                </ul>
                <p class="text-left">
                  <strong>Guindauto:</strong>
                  <br/><?php the_sub_field('caminhoes_guindauto') ?>
                </p>
                <?php
                elseif($caminhao_tipo == 'Pipa'):
                ?>
                <ul>
                  <li>
                    <img src="https://scava.com.br/wp-content/uploads/2021/03/engine.png" alt="força"/>
                    <?php the_sub_field('caminhoes_motor') ?>
                  </li>
                </ul>
                <p class="text-left">
                  <strong>Capacidade:</strong>
                  <br/><?php the_sub_field('caminhoes_capacidade_tanque') ?>
                </p>
                <?php
                endif;
              ?>
            </div>
            <div class="botao-orcamento">
              <div class="buttom" style="margin: 30px 0px;">
                  <a href="https://api.whatsapp.com/send?phone=5527998401521&text=Olá! Quero fazer um orçamento desse produto: Caminhão <?php the_sub_field('caminhoes_modelo_tipo') ?>" class="btn btn-success" target="_blank">Solicitar Orçamento</a>
              </div>
            </div>
          </div>
        </div>
      
      <?php 
        endwhile; 
      ?>
      </div>
      <?php
			  else:
			  endif; 
	  ?>
  </div>