<?php

if ( ! function_exists( 'default_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function default_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Default Theme, use a find and replace
		 * to change 'default_theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'default_theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primary', 'default_theme' ),
			)
		);

		// add class in li and a link
		function add_menu_link_class( $atts, $item, $args ) {
			if (property_exists($args, 'link_class')) {
			  $atts['class'] = $args->link_class;
			}
			return $atts;
		}
		add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );

		function add_menu_list_item_class($classes, $item, $args) {
			if (property_exists($args, 'list_item_class')) {
				$classes[] = $args->list_item_class;
			}
			return $classes;
		}
		add_filter('nav_menu_css_class', 'add_menu_list_item_class', 1, 3);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 83,
				'width'       => 300,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'default_theme' ),
					'shortName' => __( 'S', 'default_theme' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'default_theme' ),
					'shortName' => __( 'M', 'default_theme' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'default_theme' ),
					'shortName' => __( 'L', 'default_theme' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'default_theme' ),
					'shortName' => __( 'XL', 'default_theme' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	} // END IF(function_exists( 'default_theme_setup'))
endif;

add_action( 'after_setup_theme', 'default_theme_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function default_theme_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Footer', 'default_theme' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'default_theme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'default_theme_widgets_init' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function default_theme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'default_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'default_theme_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function default_theme_scripts() {

	wp_enqueue_style( 'default_theme-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );

	wp_style_add_data( 'default_theme-style', 'rtl', 'replace' );

	if ( has_nav_menu( 'menu-1' ) ) {
		wp_enqueue_script( 'default-jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/assets/js/fancybox.pack.js', array('jquery'), $version, true );
		wp_enqueue_script( 'default_theme-jquery-vide', get_template_directory_uri() . '/assets/js/jquery.vide.js', array('jquery'), $version, true );
		wp_enqueue_script( 'default_theme-jquery-counterup', get_template_directory_uri() . '/assets/js/jquery.counterup.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jquery-cubeportfolio', get_template_directory_uri() . '/assets/js/jquery.cubeportfolio.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jarallax', get_template_directory_uri() . '/assets/js/jarallax.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jquery-themepunch', get_template_directory_uri() . '/assets/js/themepunch/jquery.themepunch.revolution.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jquery-themepunch-tools', get_template_directory_uri() . '/assets/js/themepunch/jquery.themepunch.tools.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jquery-themepunch-layeranimation', get_template_directory_uri() . '/assets/js/themepunch/revolution.extension.layeranimation.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jquery-themepunch-navigation', get_template_directory_uri() . '/assets/js/themepunch/revolution.extension.navigation.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jquery-themepunch-parallax', get_template_directory_uri() . '/assets/js/themepunch/revolution.extension.parallax.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jquery-themepunch-slideanims', get_template_directory_uri() . '/assets/js/themepunch/revolution.extension.slideanims.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'jquery-themepunch-video', get_template_directory_uri() . '/assets/js/themepunch/revolution.extension.video.min.js', array('jquery'), $version, true );
		wp_enqueue_script( 'default-theme-tab-slider', get_template_directory_uri() . '/assets/js/tab_slider.js', array('jquery'), $version, true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'default_theme_scripts' );

/**
 * Default image size.
 */
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'carrossel', 1920, 1080, true);
	add_image_size( 'services-carrossel', 327, 155, true);
	add_image_size( 'recentes-carrossel', 380, 330, true);
	add_image_size( 'equip-carrossel', 250, 150, true);
    add_image_size( 'sobre-home', 555, 312, true);
    add_image_size( 'atuacao-thumb', 360, 208, true);
    add_image_size( 'informacoes-contato', 1920, 1080, true);
    add_image_size( 'noticias-thumb', 265, 247, true);
    add_image_size( 'banner-internas', 1920, 271, true);
    add_image_size( 'pilares', 431, 463, true);
    add_image_size( 'conselho', 285, 300, true);
    add_image_size( 'noticias-atuacao', 780, 400, true);
}

/**
 * Aditional content in ACF.
 */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Conteúdos Adicionais',
        'menu_title'    => 'Conteúdos Adicionais',
        'menu_slug'     => 'conteudos-adicionais',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));

	
}

/**
 * Alter Login Background Image, Color and Login logo image.
 */
function custom_login_logo() {
?>
	<style type="text/css">
		body{
			background-image:url('<?php echo the_field('login_body_background_image','options');?>');
			background-color: <?php echo the_field('login_body_background_color','options');?> !important;
			background-size: cover;
    		background-repeat: no-repeat;
		}
		.login h1 a { 
			background-image:url("<?php echo the_field('login_logo','options');?>") !important; 
			background-size: 250px auto; 
			width: 320px;
			height: 90px; 
		}
	</style>
<?php
}
add_action('login_head', 'custom_login_logo');

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function default_theme_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'default_theme_skip_link_focus_fix' );


/*
*
*
* FUNCTIONS ADDITIONALS
*
*
*/

// Remove a versão do WP no front
remove_action('wp_head','wp_generator');


/*
// Add suporte aos formatos de post
//add_theme_support( 'post-formats', array( 'gallery', 'image', 'video' ) );
//add_theme_support( 'post-thumbnails' );

// Não adiciona o jquery e scripts padrões do WP
function my_script() {
    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', get_bloginfo('template_url').'/assets/js/jquery.min.js', false, NULL);
        wp_enqueue_script('jquery');
    }
}
add_action('init', 'my_script');

// Adiciona suporte a widgets

/**
 * Register our sidebars and widgetized areas.
 *
 */
/*if (function_exists('register_sidebar')) {

    register_sidebar(array(
        'name' => 'Sidebar Calendário',
        'id'   => 'sidebar-calendario',
        'description'   => 'Aqui pode inserir o calendário.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

}

// Registra o menu
register_nav_menus( 
    array (
        'menu-principal' => __( 'Menu Principal', 'Caldas' )
    )
\);


/*-----------------------------------------------------------------------------------*/
/* Configurações do tema
/*-----------------------------------------------------------------------------------*/

// html5 support
//add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

//title tag
//add_theme_support( 'title-tag' );


/*-----------------------------------------------------------------------------------*/
/* Paginação
/*-----------------------------------------------------------------------------------*

function attach_pagination($url){
    global $wp_query;
    $page = max(get_query_var('paged'),1);
    $page_link = get_pagenum_link();
    $big = 999999999; // need an unlikely integer
    if(get_query_var('paged') == 0) $page_link .= '1/';

    $page_links = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'prev_next'    => false,
        'prev_text' => '',
        'next_text' => '',
        'total' => $wp_query->max_num_pages,
        'current' => $page,
        'type' => 'array'
    ));

    if ( $page_links ) {

    echo '<div class="post-pagination">';

        if(get_previous_posts_link()) {
			echo '<a class="post-pagination__prev" href="' . get_bloginfo('url') . $url . '"><i class="fas fa-angle-left"></i></a>';
        }
        foreach($page_links as $link) {
            echo   $link  ;
        }
        if(get_next_posts_link()) {
			echo '<a class="post-pagination__next" href="' . get_bloginfo('url') . $url . 'page/' . $wp_query->max_num_pages . '/' . '"><i class="fas fa-angle-right"></i></a>';
        }
    echo '</div>';
    }
}

// adiciona o nome do slug a função body_class
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_name;
    }
    return $classes;
}

//add_filter( 'body_class', 'add_slug_body_class' );


// tamanho de imagens personalizadas
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'carrossel', 1920, 1080, true);
    add_image_size( 'sobre-home', 555, 312, true);
    add_image_size( 'atuacao-thumb', 360, 208, true);
    add_image_size( 'informacoes-contato', 1920, 1080, true);
    add_image_size( 'noticias-thumb', 265, 247, true);
    add_image_size( 'banner-internas', 1920, 271, true);
    add_image_size( 'pilares', 431, 463, true);
    add_image_size( 'conselho', 285, 300, true);
    add_image_size( 'noticias-atuacao', 780, 400, true);
}

/* Options Tema 
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Conteúdos Adicionais',
        'menu_title'    => 'Conteúdos Adicionais',
        'menu_slug'     => 'conteudos-adicionais',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));

}


/*function edit_admin_menus() {
    global $menu;
    global $submenu;

    $menu[5][0] = 'Artigos';
    $submenu['edit.php'][5][0] = 'Todos os Artigos';
}
add_action( 'admin_menu', 'edit_admin_menus' );*/

// custom post types

// function post_type_cases() {
//     $args = array(
//         'labels' => array(
//             'name' => 'Cases',
//             'singular_name' => 'Case',
//             'add_new' => 'Adicionar nova',
//             'add_new_item' => 'Adicionar novo item',
//             'edit_item' => 'Editar item',
//             'new_item' => 'Novo item',
//             'view' => 'Ver',
//             'view_item' => 'Ver itens',
//             'search_items' => 'Procurar itens',
//             'not_found' => 'Nenhuma item encontrado',
//             'not_found_in_trash' => 'Nenhuma item encontrado na lixeira'
//         ),
//         'description' => 'Esse post type é para os cases do site',
//         'menu_position' => 8,
//         'menu_icon' => 'dashicons-editor-kitchensink',
//         'public' => true,
//         'show_ui' => true,
//         'query_var' => true,
//         'has_archive' => false,
//         'rewrite' => array( 'slug' => 'case' ),
//         'supports' => array('title','editor','excerpt')
//     );


//     register_post_type( 'case' , $args );

// }

// add_action('init', 'post_type_cases');

// function novas_taxonomias() {
//     register_taxonomy('case_mercado',array('case'), array(
//         'hierarchical' => true,
//         'label' => 'Mercado',
//         'show_ui' => true,
//         'query_var' => true,
//         'rewrite' => array( 'slug' => 'case-mercado' ),
//     ));
//     register_taxonomy('case_fabricante',array('case'), array(
//         'hierarchical' => true,
//         'label' => 'Fabricante',
//         'show_ui' => true,
//         'query_var' => true,
//         'rewrite' => array( 'slug' => 'case-fabricante' ),
//     ));
// }
// add_action('init', 'novas_taxonomias', 0);


// Funções do Tema

//LIMITAR OS CARACTERES DO THE_EXCERTP() NO WORDPRESS
/*
function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);

    if (count($excerpt)>=$limit) {

        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';

    } else {

        $excerpt = implode(" ",$excerpt);
    }

    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

    return $excerpt;
}

function cutText($limit, $s) {
    $max_length = $limit;

    if (strlen($s) > $max_length)
    {
        $offset = ($max_length - 3) - strlen($s);
        $s = substr($s, 0, strrpos($s, ' ', $offset)) . '...';
    }

    return $s;
}

// Verifica se a página é filha
function is_tree($pid) {
    global $post;

    if ( is_page($pid) )
        return true;

    $anc = get_post_ancestors( $post->ID );
    foreach ( $anc as $ancestor ) {
        if( is_page() && $ancestor == $pid ) {
            return true;
        }
    }
    return false;
}


// Retorna a Slug da Pag
function the_slug($echo=true){
	$slug = basename(get_permalink());
	do_action('before_slug', $slug);
	$slug = apply_filters('slug_filter', $slug);
	if( $echo ) echo $slug;
		do_action('after_slug', $slug);
	return $slug;
}

/*add_filter( 'body_class', 'class_slug_page' );
function class_slug_page( $classes )
{
	global $post;

	// add 'post_name' to the $classes array
	$classes[] = $post->post_name;
	// return the $classes array
	return $classes;
}

// Alterar a palavra para o singular

function depluralize($word){
    // Here is the list of rules. To add a scenario,
    // Add the plural ending as the key and the singular
    // ending as the value for that key. This could be
    // turned into a preg_replace and probably will be
    // eventually, but for now, this is what it is.
    //
    // Note: The first rule has a value of false since
    // we don't want to mess with words that end with
    // double 's'. We normally wouldn't have to create
    // rules for words we don't want to mess with, but
    // the last rule (s) would catch double (ss) words
    // if we didn't stop before it got to that rule.
    $rules = array(
        'ss' => false,
        'os' => 'o',
        'ies' => 'y',
        'xes' => 'x',
        'oes' => 'o',
        'ies' => 'y',
        'ves' => 'f',
        's' => '');
    // Loop through all the rules and do the replacement.
    foreach(array_keys($rules) as $key){
        // If the end of the word doesn't match the key,
        // it's not a candidate for replacement. Move on
        // to the next plural ending.
        if(substr($word, (strlen($key) * -1)) != $key)
            continue;
        // If the value of the key is false, stop looping
        // and return the original version of the word.
        if($key === false)
            return $word;
        // We've made it this far, so we can do the
        // replacement.
        return substr($word, 0, strlen($word) - strlen($key)) . $rules[$key];
    }
    return $word;
}

/* Função para tirar acentos e espaços*/

/*function clearwords($variavel) {
	$variavel_limpa = strtolower( ereg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($variavel)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) );
	return $variavel_limpa;
}

function traducaoTexto ($array) {

    $traducao = $array;

    $idioma = get_locale();

    switch ($idioma) {
        case 'pt_BR':
            $idiomaNum = 0;
            break;
        case 'en_US':
            $idiomaNum = 1;
            break;
        default:
             $idiomaNum = 0;
            break;
    }

    return $traducao[$idiomaNum];
}


// Função para os compartilhamentos de todos os posts

function  addSharePost($link, $titulo/*, $imagem*//*) {
    //render
    $str = '';

    //facebook
    $str .= '<a class="blog-one__share" original-title="FACEBOOK" href="http://www.facebook.com/sharer.php?u=' . urlencode( $link ) . '" onclick="window.open(this.href, \'mywin\',\'left=50,top=50,width=600,height=350,toolbar=0\'); return false;"><i class="fab fa-facebook"></i></a>';

    //twitter
    $str .= '<a class="blog-one__share" original-title="TWITTER" href="https://twitter.com/intent/tweet?text=' . urlencode( strip_tags( $titulo ) ) . '&amp;url=' . urlencode( $link ) . '&amp;via=' . urlencode( $twitter_user ? $twitter_user : get_bloginfo( 'name' ) ) . '" onclick="window.open(this.href, \'mywin\',\'left=50,top=50,width=600,height=350,toolbar=0\'); return false;"><i class="fab fa-twitter"></i></a>';

    //google plus
    /*$str .= '<a original-title="GOOGLE" href="http://plus.google.com/share?url=' . urlencode( $link ) . '" onclick="window.open(this.href, \'mywin\',\'left=50,top=50,width=600,height=350,toolbar=0\'); return false;"><i class="fab fa-google"></i></a>';*/

    //pinterest
    /*$str .= '<a original-title="PINTEREST" href="http://pinterest.com/pin/create/button/?url=' . urlencode( $link ) . '&amp;media=' . ( ! empty( $imagem ) ? $imagem : '' ) . '" onclick="window.open(this.href, \'mywin\',\'left=50,top=50,width=600,height=350,toolbar=0\'); return false;"><i class="fa fa-pinterest"></i></a>';*/

	//linkedin
	/*
    $str .= '<a class="blog-one__share" original-title="LINKEDIN" href="http://linkedin.com/shareArticle?mini=true&amp;url=' . urlencode( $link ) . '&amp;title=' . urlencode( strip_tags( $titulo ) ) . '" onclick="window.open(this.href, \'mywin\',\'left=50,top=50,width=600,height=350,toolbar=0\'); return false;"><i class="fab fa-linkedin-in"></i></a>';


    return $str;
}

/* Parse the video uri/url to determine the video type/source and the video id *//*
function parse_video_uri( $url ) {

	// Parse the url
	$parse = parse_url( $url );

	// Set blank variables
	$video_type = '';
	$video_id = '';

	// Url is http://youtu.be/xxxx
	if ( $parse['host'] == 'youtu.be' ) {

		$video_type = 'youtube';

		$video_id = ltrim( $parse['path'],'/' );

	}

	// Url is http://www.youtube.com/watch?v=xxxx
	// or http://www.youtube.com/watch?feature=player_embedded&v=xxx
	// or http://www.youtube.com/embed/xxxx
	if ( ( $parse['host'] == 'youtube.com' ) || ( $parse['host'] == 'www.youtube.com' ) ) {

		$video_type = 'youtube';

		parse_str( $parse['query'] );

		$video_id = $v;

		if ( !empty( $feature ) )
			$video_id = end( explode( 'v=', $parse['query'] ) );

		if ( strpos( $parse['path'], 'embed' ) == 1 )
			$video_id = end( explode( '/', $parse['path'] ) );

	}

	// Url is http://www.vimeo.com
	if ( ( $parse['host'] == 'vimeo.com' ) || ( $parse['host'] == 'www.vimeo.com' ) ) {

		$video_type = 'vimeo';

		$video_id = ltrim( $parse['path'],'/' );

	}
	$host_names = explode(".", $parse['host'] );
	$rebuild = ( ! empty( $host_names[1] ) ? $host_names[1] : '') . '.' . ( ! empty($host_names[2] ) ? $host_names[2] : '');
	// Url is an oembed url wistia.com
	if ( ( $rebuild == 'wistia.com' ) || ( $rebuild == 'wi.st.com' ) ) {

		$video_type = 'wistia';

		if ( strpos( $parse['path'], 'medias' ) == 1 )
				$video_id = end( explode( '/', $parse['path'] ) );

	}

	// If recognised type return video array
	if ( !empty( $video_type ) ) {

		$video_array = array(
			'type' => $video_type,
			'id' => $video_id
		);

		return $video_array;

	} else {

		return false;

	}

}

/* Ajusta o funcionamento da páginação nas páginas de Archivo e Busca *//*
function custom_posts_per_page( $query ) {

    if ( $query->is_archive() ) {
        set_query_var('posts_per_page', 5);
    }

	if ( $query->is_search() ) {
        set_query_var('posts_per_page', 5);
    }
}
//add_action( 'pre_get_posts', 'custom_posts_per_page' );


/* Customize wp get archives */

/*add_filter( 'get_archives_link', function( $link_html, $url, $text, $format, $before, $after ) {

    if ( 'custom' == $format ) {
        $link_html = "\t<li><a href='$url' class='clearfix'><span class='inner clearfix'><span class='txt1'>$text</span></a></li>\n";
    }

    return $link_html;

}, 10, 6 );*/


/* add_filter( 'wp_nav_menu_objects', 'add_has_children_to_nav_items' );

function add_has_children_to_nav_items( $items )
{
    $parents = wp_list_pluck( $items, 'menu_item_parent');

    foreach ( $items as $item )
        in_array( $item->ID, $parents ) && $item->classes[] = 'dropdown';

    return $items;
} 



// Adiciona Classe a tag a do menu 
function add_menu_link_class($atts, $item, $args)
{
    $atts['class'] = 'nav-link';
    return $atts;
}
//add_filter('nav_menu_link_attributes', 'add_menu_link_class', 1, 3);

// Adiciona Classe a li do menu
function add_classes_on_li($classes, $item, $args) {
  $classes[] = 'nav-item';
  return $classes;
}
//add_filter('nav_menu_css_class','add_classes_on_li',1,3);

function replace_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/','/ class="dropdown-menu" /',$menu);
    return $menu;
  }
  //add_filter('wp_nav_menu','replace_submenu_class');


function force_ssl()
{
    // Specify ID of page to be viewed on SSL connection

    if (!is_ssl ())
    {
      header('HTTP/1.1 301 Moved Permanently');
      header("Location: https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
      exit();
    }

    // All other pages must not be https

    /*else if (!is_page(9) && is_ssl() )

    {
        header('Location: http://' . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
        exit();
    }*//*
}
//force_ssl();


function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return str_replace(["-", "–"], '', $string);
    // Removes special chars.
 }


 */



