  <div class="container maq-container">
      <?php 
        if(have_rows('carrossel_equip_roloscompactadores')) : 
	  ?>
		<h3 class="maq-title" id="rolocompactador">
      <i class="fa fa-angle-right"></i> ROLOS COMPACTADORES
		</h3>
	  <?php
            while(have_rows('carrossel_equip_roloscompactadores')) : 
                the_row(); 
                $attachment_id = get_sub_field('roloscompactadores_imagem');
                $imagem = wp_get_attachment_image_src( $attachment_id, 'equip-carrossel' );
       ?>
        <div class="col-md-4 col-sm-6 col-xs-12 text-center">
          <div class="chose_box">
            <img src="<?php echo $imagem[0] ?>"/>
            <div class="content">
              <h4 class="maq-item-title"><strong><?php the_sub_field('roloscompactadores_marca') ?></strong> - <?php the_sub_field('roloscompactadores_modelo') ?></h4>
              <ul>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/weight-kg-e1615414781710.png" alt="peso"/>
                  <?php the_sub_field('roloscompactadores_peso') ?>
                </li>
                <li>
                  <img src="https://scava.com.br/wp-content/uploads/2021/03/engine.png" alt="força"/>
                  <?php the_sub_field('roloscompactadores_motor') ?>
                </li>
              </ul>
              <p class="text-left implementos">
                <strong style="font-size:14px">Implementos:</strong>
                <br/><?php the_sub_field('roloscompactadores_implementos') ?>
              </p>
              <p class="text-left">
                <strong style="font-size:14px">Cabine:</strong>
                <br/><?php the_sub_field('roloscompactadores_cabine') ?>
              </p>
              <p class="text-left">
                <strong>Largura do rolo:</strong>
                <br/><?php the_sub_field('roloscompactadores_largura_do_rolo') ?>
              </p>
            </div>
            <div class="botao-orcamento">
              <div class="buttom" style="margin: 30px 0px;">
                  <a href="https://api.whatsapp.com/send?phone=5527998401521&text=Olá! Quero fazer um orçamento desse produto: ROLO COMPACTADOR" class="btn btn-success" target="_blank">Solicitar Orçamento</a>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; 
			else:
			endif; 
	  ?>
  </div>