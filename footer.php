<!--========== FOOTER ==========-->
<footer id="index1-footer">
	  <!--
    <div class="container">

    	<div class="row">
        	
        <div class="col-md-12 text-center">
        	<div class="social-icons mb30">
            <ul>
              <li><a href="#."><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
	            <li><a href="#."><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#."><i aria-hidden="true" class="fa fa-dribbble"></i></a></li>
              <li><a href="#."><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              <li><a href="#."><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
          
      	<div class="col-md-12 text-center">
          <ul class="footer_link mb50">
            <li><a href="#.">Home</a></li>
            <li><a href="#.">About Us</a></li>
            <li><a href="#.">Services</a></li>
            <li><a href="#.">Projects</a></li>
            <li><a href="#.">Blog</a></li>
            <li><a href="#.">Shop</a></li>
            <li><a href="#.">Contact us</a></li>
          </ul>
        </div>

      </div>

    </div>
    !-->
    <div class="index1-footer-b">
    	<div class="container">
        
        	<div class="col-md-6">
            	<p class="foot-p">Todos direitos reservados © 2021 SCAVA por <a href="http://www.agenciakairos.com.br">Agência Kairós</a></p>
            </div>
            
            <div class="col-md-6 text-right">
              <div class="social-icons" style="margin-top: -10px;">
                <ul>
                  <li><a href="mailto:<?php the_field('email_da_empresa','options');?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                  <li><a href="https://wa.me/<?php the_field('link_whatsapp_da_empresa','options');?>"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                </ul>
              </div>
            	<!--
              <ul class="list-inline">
                <li> <a href="#">FAQ</a> </li>
                <li>|</li>
                <li> <a href="#">Help Desk</a> </li>
                <li>|</li>
                <li> <a href="#">Support</a> </li>
              </ul>
              !-->
            </div>
            
        </div>
    </div>
</footer>
<div class="whatsappFloating">
	<a href="https://wa.me/<?php the_field('link_whatsapp_da_empresa','options');?>" target="_blank" title="Chame no Whatsapp">
		<button id="btn-whatsapp" class=""></button>
		<span class="whatsappFloatingText">
			<img class="whatsappFloatingImg" src="https://scava.com.br/wp-content/uploads/2021/02/led-alert.gif">
			Estamos Online		</span>
	</a>
</div>
<!--========== #/FOOTER ==========-->

<!--========== REQUIRED JS ==========--> 


<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url'); ?>/assets/js/function.js?id=<?php echo time(); ?>"></script>

</body>
</html>
